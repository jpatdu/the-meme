<?php 
if(isset($_REQUEST['txn_id'])){
    session_start();
    $_SESSION['txn_id'] = $_REQUEST['txn_id'];
}
else{
    header('Location: http://thememe.online');
    die();
}
?>

<html>
    <head>
        <title>Please wait...</title>
        <style>
            body{
                background: #000;
                color: #fff;
            }
        </style>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body>
        <center>
            <img src="images/ajax-loader.gif"/>
            <h4>Please wait, this will take a couple of seconds...</h4>
        </center>
    </body>

<script type="text/javascript">
    function redirect(){
        window.location = "https://thememe.online/paypal-ipn-php/meme.php";
    }
    
    setTimeout(redirect, 12000);
</script>
</html>