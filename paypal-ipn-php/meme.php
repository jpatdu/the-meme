<?php 
session_start();
if(isset($_SESSION['txn_id'])){
    $txn_id = $_SESSION['txn_id'];
    require_once('connection.php');
    $result = mysqli_query($con, "SELECT * FROM ipn_data_tbl WHERE txn_id = '$txn_id'");
    $row_cnt = mysqli_num_rows($result);
    mysqli_close($con);
    
    if($row_cnt < 1){
        echo "Transaction no. ".$txn_id." not found. Please try reloading this page. If that doesn't work, please contact seller and indicate the transaction no. in your message.";
        die();
    }
    else {        
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.imgur.com/3/gallery/search/".mt_rand(1,100)."?q=memes",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Client-ID f8d167353fe4156"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $json = json_decode($response, true);
            $count_results = count($json['data']) - 1;
            $resNum = mt_rand(0,$count_results);
            
            $title = $json['data'][$resNum]['title'];
            $desc = $json['data'][$resNum]['description'];
            $link = $imgLink = $json['data'][$resNum]['link'];
            $imgLink = "";
            $imgTitle = "";
            $imgDesc = "";
            
            if($json['data'][$resNum]['is_album'] = 1){
                $count_img = count($json['data'][$resNum]['images']) - 1;
                $imgNum = mt_rand(0,$count_img);
            
                $imgLink = $json['data'][$resNum]['images'][$imgNum]['link'];
                $imgTitle = $json['data'][$resNum]['images'][$imgNum]['title'];
                $imgDesc = $json['data'][$resNum]['images'][$imgNum]['description'];
            } else {
                $imgLink = $json['data'][$resNum]['link'];
            }
        }
    }
}
else{
    echo "Transaction number not found. Please try reloading this page. If that doesn't work, please contact seller.";
    die();
}
?>

<html>
    <head>
        <title>The Meme</title>
        <style>
            body{
                background: #000;
                color: #fff;
            }
            img{
                max-width: 100%;
            }
            .header{
                height: 100vh;
                padding-top: 42vh;
            }
            .meme{
                min-height: 100vh;
                padding: 8%;
            }
        </style>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112463171-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-112463171-1');
        </script>
    </head>
    <body>
        <center>
            <div class="header">
                <h1>Scroll to see The Meme</h1>
            </div>
            <div class="meme">
                <h1>"<?php echo $title ?>"</h1>
                <p><?php echo $desc ?></p>
                <h6><?php echo $imgTitle ?></h6>
                <p><?php echo $imgDesc ?></p>
                <img src="<?php echo $imgLink ?>">
                <p>Broken image? <a href="<?php echo $link ?>">Source</a></p>
                <a href="https://thememe.online">Return to homepage</a>
            </div>
        </center>
    </body>
</html>