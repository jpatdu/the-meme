<?php
require_once('paypal-ipn-php/connection.php');
$result = mysqli_query($con, "SELECT * FROM ipn_data_tbl");
$row_cnt = mysqli_num_rows($result);
$price = 1 + $row_cnt;
mysqli_close($con);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>The Meme</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link type="text/css" rel="stylesheet" href="animate.css"/>
        <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="styles.css"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112463171-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-112463171-1');
        </script>
    </head>
    
    <body>
        <div class="header">
            <div class="splash col-xs-12">
                <div class="background">
                    <div class="col-xs-12"><b>What</b></div>
                    <div class="col-sm-6 col-xs-12"><b>is</b></div>
                    <div class="col-sm-6 col-xs-12"><b>it?</b></div>
                </div>
                <div class="text">
                    <h2><i>Look! Here lies</i></h2>
                    <h1><b>The Meme</b></h1>
                    <h2 style="color:pink !important;"><i>"The Mystery of the Internet"</i></h2>
                </div>
                <div class="scrollCTA">
                    <h2 class="animated infinite bounce"><b><i>Scroll down to find out</i></b></h2>
                </div>
            </div>
            <div class="inter col-xs-12">
                <div class="text">
                    <h2 class="animated infinite rubberBand"><i>"What is it?"</i></h2>
                </div>
                <div class="rellax col-xs-6 col-sm-4" data-rellax-speed="1"><b>?</b></div>
                <div class="rellax col-xs-6 col-sm-4" data-rellax-speed="3"><b>?</b></div>
                <div class="rellax col-xs-6 col-sm-4" data-rellax-speed="3"><b>?</b></div>
                <div class="rellax col-xs-6 col-sm-4" data-rellax-speed="3"><b>?</b></div>
                <div class="rellax col-xs-6 col-sm-4" data-rellax-speed="3"><b>?</b></div>
                <div class="rellax col-xs-6 col-sm-4" data-rellax-speed="1"><b>?</b></div>
            </div>
        </div>
        <div class="content">
            <h2 data-aos="fade-left">"Special"</h2>
            <h2 data-aos="fade-right" data-aos-delay="600">"A divine gift from the meme gods"</h2>
            <h2 data-aos="fade-left" data-aos-delay="1200">"Enigmatic"</h2>
            <h1 data-aos="fade-right" data-aos-delay="1800" data-aos-duration=
"1200"><i><b>"dank"</b></i></h1>
            <br>
            <div data-aos="flip-left" data-aos-delay="3000">
                <p>Before you get to see The Meme, an offering is required.</p>
                <p>Required offering goes up by $1 for every person that offers.</p>
                <p>You will, in exchange, get to see The Meme, a divine creation unique to you.</p>
                <p>(In other words, The Meme is unique to each and every person. Your meme is different from my meme. It's a mystery. It's a surprise.)</p>
                <br>
                <p>The current offering is $<?php echo $price;?>.</p>
                <p>That means <?php echo $row_cnt;?> people have already offered.</p>
                <p>Hurry, before the required offering goes up.</p>
                <form action="paypal-ipn-php/process.php" method="post">
                    <input type="submit" name="submit" value="Send your offering" >
                </form>
            </div>
            <div class="footer">
                Coded with &hearts; by a very broke bloke
            </div>
        </div>
        
        <script src="rellax.js"></script>
        <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
        <script src="app.js"></script>
    </body>
</html>